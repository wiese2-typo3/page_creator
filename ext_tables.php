<?php

defined("TYPO3_MODE") || die("Access denied.");

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
    "Wiese2.PageCreator",
    "web",
    "PageCreator",
    "after:ts",
    [
        "PageCreator" => "list,create"
    ],
    [
        "access" => "user,group",
        "icon" => "EXT:$_EXTKEY/ext_icon.png",
        "labels" => "LLL:EXT:$_EXTKEY/Resources/Private/Language/locallang_db.xlf"
    ]
);

if (TYPO3_MODE == 'BE') {
    $TBE_STYLES['skins'][$_EXTKEY]['name'] = $_EXTKEY;
    $TBE_STYLES['skins'][$_EXTKEY]['stylesheetDirectories']['structure'] = "EXT:$_EXTKEY/Resources/Public/CSS/Backend";
}