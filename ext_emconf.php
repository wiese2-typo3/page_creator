<?php

$EM_CONF[$_EXTKEY] = [
    "title" => "Page Creator",
    "description" => "Page Creator is a tool for creating many pages quickly and easily",
    "category" => "misc",
    "author" => "milan44",
    "author_email" => "milanbartky@gmail.com",
    "state" => "stable",
    "version" => "1.0.1",
    "constraints" => [
        "depends" => [
            "typo3" => "9.5.0-9.5.99",
            'php' => '',
            'fluid_styled_content' => ''
        ],
        "conflicts" => [],
        "suggests" => [],
    ]
];