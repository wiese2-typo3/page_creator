define(['jquery'], function($) {
    function uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
    function validateField(field, markErrors) {
        var value = field.val().trim();
        if (value === "" && field[0].hasAttribute('required')) {
            if (markErrors) {
                field.closest('.form-group').addClass('has-error');
            }
            return false;
        }

        field.closest('.form-group').removeClass('has-error');
        return value;
    }
    function validatePageElements(markErrors) {
        var invalidFieldsExist = false;

        var pages = [];
        $('.page-element').each(function() {
            var title = validateField($('input[name="title"]', this), markErrors);
            if (title === false) {
                invalidFieldsExist = true;
            }

            var doktype = validateField($('select[name="doktype"]', this), markErrors);
            if (doktype === false) {
                invalidFieldsExist = true;
            }

            var abstract = validateField($('textarea[name="abstract"]', this), markErrors);
            if (abstract === false) {
                invalidFieldsExist = true;
            }

            pages.push({
                title: title,
                doktype: doktype,
                abstract: abstract
            });
        });
        if (invalidFieldsExist) {
            return false;
        }
        return pages;
    }

    var pageElement = $('.page-creator .page-element').clone();

    $('.page-creator').on('click', '.addNewPage', function(e) {
        e.preventDefault();

        var element = pageElement.clone();
        var id = uuidv4();
        element.attr('id', id);

        $('.page-creator #pages').append(element);

        setTimeout(function() {
            $('input[name="title"]', element).focus();
        }, 100);

        location.href = "#" + id;
    });

    $('.page-creator').on('click', '.saveAll', function(e) {
        e.preventDefault();

        var pages = validatePageElements(true);
        if (pages && pages.length > 0) {
            $('.page-creator .loader-overlay').addClass('active');

            $('#pagesField').val(JSON.stringify(pages));

            $('#pageForm').submit();
        }
    });

    $('.page-creator').on('keyup', '.form-control', function() {
        validatePageElements(true);
    });
    $('.page-creator').on('change', 'select[name="doktype"]', function() {
        var icon = $('option:selected', this).data('icon');
        var group = $(this).closest('.input-group');
        var iconWrapper = $('.doktype-icon', group);

        iconWrapper.attr('title', $('option:selected', this).text());
        iconWrapper.html(icon);
    });

    $('.page-creator').on('click', '.delete-page', function(e) {
        e.preventDefault();

        $(this).closest('.page-element').remove();
    });

    $(document).ready(function() {
        $('input[name="title"]').focus();
    });
});