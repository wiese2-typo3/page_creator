# Page Creator

Page Creator is a tool for creating many pages quickly and easily.

### Installation

Add the "Page Creator" TypoScript in your Template (after fluid_styled_content).

### Usage

Select the Page Creator Module in the Backend.  
![select_in_backend.png](doc_img/select_in_backend.png)

If you don't have a page selected in the pagetree it will show this message:  
![no_page_selected.png](doc_img/no_page_selected.png)

Click on a page in the pagetree to add subpages to it.  
![page_selected.png](doc_img/page_selected.png)

You can add new pages with the "Add new" button. When you're done adding all your pages, click the "Save" button to create them.