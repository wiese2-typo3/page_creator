<?php

defined("TYPO3_MODE") or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    "<INCLUDE_TYPOSCRIPT: source=\"DIR:EXT:$_EXTKEY/Configuration/TSconfig\">"
);

$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$icons = [
    "pagecreator-icon" => "EXT:$_EXTKEY/ext_icon.png",
];

foreach ($icons as $iconIdentifier => $path) {
    $iconRegistry->registerIcon(
        $iconIdentifier,
        \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
        ["source" => $path]
    );
}

if (TYPO3_MODE == 'BE') {
    (function(\TYPO3\CMS\Core\Page\PageRenderer $pageRenderer) {
        $pageRenderer->loadRequireJsModule('TYPO3/CMS/PageCreator/Backend/PageCreator');
    })(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\PageRenderer::class));
}