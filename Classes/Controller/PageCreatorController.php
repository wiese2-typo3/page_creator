<?php
declare(strict_types=1);

namespace Wiese2\PageCreator\Controller;

/*
 * Copyright notice
 *
 * (c) 2020 milan44 <milanbartky@gmail.com>
 * All rights reserved
 */


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class PageCreatorController extends ActionController {
    const ignoreDoktypes = [6, 7, 254, 255, 199];

    /**
     * @var \Wiese2\PageCreator\Domain\Repository\PageRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $pageRepository;

    public function listAction() {
        $page = $this->pageRepository->findByUid($this->getCurrentUid());

        $iconFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            \TYPO3\CMS\Core\Imaging\IconFactory::class
        );

        $doktypes = [];
        foreach($GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'] as $doktype) {
            if (!isset($doktype[2]) || in_array(intval($doktype[1]), self::ignoreDoktypes)) {
                continue;
            }
            $doktypes[] = [
                'id' => $doktype[1],
                'label' => $doktype[0],
                'icon' => $iconFactory->getIcon($doktype[2], \TYPO3\CMS\Core\Imaging\Icon::SIZE_SMALL)
            ];
        }

        $this->view->assign('page', $page);
        $this->view->assign('doktypes', $doktypes);
    }

    /**
     * @param string $pages
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function createAction(string $pages)
    {
        $pages = json_decode($pages, true);
        $pid = $this->getCurrentUid();
        $lastSorted = $this->pageRepository->findByPidLastSorted($pid)->getFirst();

        /**
         * @var \TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler
         */
        $dataHandler = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);

        $pid = $lastSorted ? -$lastSorted->getUid() : $pid;

        $index = 1;
        $data = [
            'pages' => []
        ];
        foreach($pages as $page) {
            $data['pages']['NEW_'.$index] = [
                'pid' => $pid,
                'title' => $page['title'],
                'doktype' => $page['doktype'],
                'abstract' => $page['abstract'],
                'hidden' => false
            ];

            $index++;
        }

        $TCAdefaultOverride = $this->getBackendUser()->getTSConfig('TCAdefaults')['properties'];
        if (is_array($TCAdefaultOverride)) {
            $dataHandler->setDefaultsFromUserTS($TCAdefaultOverride);
        }

        $dataHandler->reverseOrder = 1;
        $dataHandler->start($data, []);
        $dataHandler->process_datamap();
        \TYPO3\CMS\Backend\Utility\BackendUtility::setUpdateSignal('updatePageTree');
        $dataHandler->clear_cacheCmd('pages');

        $this->redirect('list');
    }

    /**
     * Returns the current BE user.
     *
     * @return \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }

    /**
     * Get the current UID.
     *
     * @return int
     */
    protected function getCurrentUid(): int
    {
        return (int)GeneralUtility::_GET('id');
    }
}