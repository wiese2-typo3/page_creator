<?php
declare(strict_types=1);

namespace Wiese2\PageCreator\Domain\Repository;

/*
 * Copyright notice
 *
 * (c) 2020 milan44 <milanbartky@gmail.com>
 * All rights reserved
 */


use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

class PageRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    function findByPidLastSorted(int $pid) : QueryResultInterface
    {
        $query = $this->createQuery();
        $query->matching(
            $query->equals('pid', $pid)
        );
        $query->setOrderings([
            'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
        ]);
        $query->setLimit(1);
        return $query->execute();
    }
}