<?php

defined("TYPO3_MODE") or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    "page_creator",
    "Configuration/TypoScript",
    "Page Creator"
);